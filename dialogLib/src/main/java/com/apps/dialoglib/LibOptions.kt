package com.apps.dialoglib

class LibOptions(val title: String, val body: String) {

    class Builder {

        private lateinit var title: String
        private lateinit var body: String

        fun setTitle(title: String): Builder {
            this.title = title
            return this;
        }

        fun setBody(body: String): Builder {
            this.body = body;
            return this;
        }

        fun create(): LibOptions {
            return LibOptions(title, body)
        }
    }
}