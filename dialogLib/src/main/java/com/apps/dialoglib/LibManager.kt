package com.apps.dialoglib

import android.content.Context
import androidx.appcompat.app.AlertDialog

class LibManager constructor(val context: Context) {

    init {
        // do some stuff
    }

    fun show(options: LibOptions) {
        val builder = AlertDialog.Builder(context)
        builder.setTitle(options.body).setMessage(options.body)
        builder.setNegativeButton("Close") { dialog, which -> dialog?.dismiss() }
        options.body
        builder.show()
    }

    companion object : SingletonHolder<LibManager, Context>(::LibManager)
}